# Installing Openshift Enterprise on Azure with IPI Method

### Prerequisites

- [Configure AZ account limits](https://docs.openshift.com/container-platform/4.10/installing/installing_azure/installing-azure-account.html#installation-azure-limits_installing-azure-account)

- [Configure the DNS zone](https://docs.openshift.com/container-platform/4.10/installing/installing_azure/installing-azure-account.html#installation-azure-network-config_installing-azure-account)

- [Require Azure roles](https://docs.openshift.com/container-platform/4.10/installing/installing_azure/installing-azure-account.html#installation-azure-permissions_installing-azure-account)

- [Creating a service principal](https://docs.openshift.com/container-platform/4.10/installing/installing_azure/installing-azure-account.html#installation-azure-service-principal_installing-azure-account)


#### 1- Create install-config.yaml

> openshift-install create install-config --dir <installation_directory> --log-level=debug
>
